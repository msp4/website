from django.http import HttpResponseRedirect
from django.http import HttpResponse

def ref(request, userid):
    response = HttpResponse('');
    response = HttpResponseRedirect('/')
    response.set_cookie('referrer', userid, domain='.msp4.me');

    return response