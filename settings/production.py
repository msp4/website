import os
from django.core.exceptions import ImproperlyConfigured

from .base import *

DEBUG = False
COMPRESS_OFFLINE = True
# COMPRESS_ROOT = MEDIA_ROOT

# Parse database configuration from $DATABASE_URL
import dj_database_url

DATABASES['default'] = dj_database_url.config()

def get_env_variable(var_name):
    """
      Get the environment variable or return exception
    """
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)

SECRET_KEY = get_env_variable("DJANGO_SECRET_KEY")